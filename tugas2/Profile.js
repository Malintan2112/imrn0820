import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image
} from 'react-native';
const Profile = () => {

    return (
        <View style={{ flex: 1 }}>
            <PhotoProfile />
            <ContainerDetail />
        </View>
    )
}
const PhotoProfile = () => {
    return (
        <View style={{ width: "100%", height: "30%", backgroundColor: "#3EC6FF", alignItems: "center", justifyContent: "center" }}>
            <Image
                style={{ width: 100, height: 100, borderRadius: 50 }}
                source={{
                    uri: 'https://reactnative.dev/img/tiny_logo.png',
                }}
            />
        </View>
    )
}
const ContainerDetail = () => {
    return (
        <View style={{ width: "100%", height: "70%", alignItems: "center" }}>
            <View style={{ width: "90%", height: "40%", elevation: 3, backgroundColor: "white", marginTop: -20, borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
                <AtributDetail title="Tanggal Lahir" value="21 Desember 1996" />
                <AtributDetail title="Nama" value="Hernawan Putra Malintan" />
                <AtributDetail title="Hobi" value="Ngoding" />
                <AtributDetail title="Email" value="malintan@gmail.com" />


            </View>
        </View>
    )
}
const AtributDetail = (props) => {
    return (
        <View style={{ padding: 10, flexDirection: "row", width: "80%", justifyContent: "space-between" }}>
            <Text >{props.title} :</Text>
            <Text >{props.value} </Text>
        </View>
    )
}

export default Profile;