import React, { useState, createContext } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import TodoList from '../../components/TodoList';
export const RootContext = createContext();
const Context = () => {
    const [input, setInput] = useState('');
    const [todos, setTodos] = useState([]);
    const handleChangeInput = (text) => {
        setInput(text)
    }
    const addTodo = () => {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "Augustus", "September", "October", "November", "December"];
        let dateObj = new Date();
        let month = monthNames[dateObj.getMonth()];
        let day = String(dateObj.getDate()).padStart(2, '0');
        let year = dateObj.getFullYear();
        let output = `${day}/${month}/${year}`;


        setTodos([...todos, { title: input, date: output, id: todos.length + 1 }]);
        setInput('');
    }
    const deleteTodo = (id) => {
        let newTodo = [...todos.filter((data) => {
            return data.id != id
        })];
        setTodos(newTodo);
    }
    return (
        <RootContext.Provider value={{ input, todos, handleChangeInput, addTodo, deleteTodo }}>
            <TodoList></TodoList>
        </RootContext.Provider>
    )
}
export default Context;