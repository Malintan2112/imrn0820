import React, { useState, useContext } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TextInput,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import { RootContext } from '../screens/Tugas4';


const TodoList = () => {
    const [text, onChangeText] = useState('');
    const [toDoList, setToDoList] = useState([]);
    const state = useContext(RootContext);


    return (
        <View style={styles.container}>
            <Text>Masukan Todolist</Text>
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: 10 }}>
                <TextInput
                    style={{ height: 60, width: "75%", borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10 }}
                    onChangeText={text => state.handleChangeInput(text)}
                    value={state.input}
                    placeholder="Input Here"
                />
                <TouchableOpacity style={styles.btnCreate} onPress={() => state.addTodo()}>
                    <Text style={{ fontSize: 40, fontWeight: "bold", color: "white" }}>
                        +
                    </Text>
                </TouchableOpacity>
            </View>
            <FlatList renderItem={(dataItem) => {
                return (
                    <View style={{ width: "100%", padding: 10, height: 50, marginVertical: 5, elevation: 3, backgroundColor: "white", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }} key={dataItem.item.id}>
                        <Text>{dataItem.item.title}</Text>
                        <Text>{dataItem.item.date}</Text>
                        <TouchableOpacity onPress={() => { state.deleteTodo(dataItem.item.id) }}>
                            <Text style={{ fontSize: 30, fontWeight: "bold", color: "red" }}>x</Text>
                        </TouchableOpacity>
                    </View>
                )
            }} data={state.todos} key={(dataItem) => { dataItem.item.id }}>
            </FlatList>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        padding: 30,
        flex: 1,

    },
    btnCreate: {
        width: 60,
        height: 60,
        backgroundColor: "teal",
        justifyContent: "center",
        alignItems: "center"
    }

})
export default TodoList;